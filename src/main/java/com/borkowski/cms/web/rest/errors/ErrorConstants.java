package com.borkowski.cms.web.rest.errors;

import java.net.URI;

public final class ErrorConstants {

    public static final String ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure";
    public static final String ERR_VALIDATION = "error.validation";
    public static final String PROBLEM_BASE_URL = "https://www.jhipster.tech/problem";
    public static final String ID_IS_NULL = "error.id-is-null";
    public static final String ID_IS_NOT_NULL = "error.id-is-not-null";
    public static final String ID_IS_NEGATIVE = "error.id-is-negative";
    public static final String MOVIE_IS_NOT_EXIST = "error.movie-not-exist";
    public static final String HALL_IS_NOT_EXIST = "error.hall-not-exist";
    public static final String ROW_WITH_ID_NOT_EXIST = "error.row-with-id-not-exist";
    public static final String SCREENING_WITH_ID_NOT_EXIST = "error.screening-with-id-not-exist";
    public static final String RESERVATION_WITH_ID_NOT_EXIST = "error.reservation-with-id-not-exist";
    public static final String COLUMN_NUMBER_IS_INVALID = "error.column-number-is-invalid";
    public static final String SEAT_IS_RESERVED = "error.choose-seat-is-reserved";
    public static final String GAP_BETWEEN_SEATS = "error.gap_between_seats";
    public static final String TIME_IS_UP = "error.is-too-late";

    public static final URI DEFAULT_TYPE = URI.create(PROBLEM_BASE_URL + "/problem-with-message");
    public static final URI CONSTRAINT_VIOLATION_TYPE = URI.create(PROBLEM_BASE_URL + "/constraint-violation");
    public static final URI PARAMETERIZED_TYPE = URI.create(PROBLEM_BASE_URL + "/parameterized");
    public static final URI ENTITY_NOT_FOUND_TYPE = URI.create(PROBLEM_BASE_URL + "/entity-not-found");
    public static final URI INVALID_PASSWORD_TYPE = URI.create(PROBLEM_BASE_URL + "/invalid-password");
    public static final URI EMAIL_ALREADY_USED_TYPE = URI.create(PROBLEM_BASE_URL + "/email-already-used");
    public static final URI LOGIN_ALREADY_USED_TYPE = URI.create(PROBLEM_BASE_URL + "/login-already-used");
    public static final URI EMAIL_NOT_FOUND_TYPE = URI.create(PROBLEM_BASE_URL + "/email-not-found");

    private ErrorConstants() {
    }
}
