package com.borkowski.cms.web.rest;

import com.borkowski.cms.domain.Hall;
import com.borkowski.cms.security.AuthoritiesConstants;
import com.borkowski.cms.service.HallService;
import com.borkowski.cms.service.dto.HallDTO;
import com.borkowski.cms.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api")
@Secured(AuthoritiesConstants.ADMIN)
public class HallResource {

    private final Logger log = LoggerFactory.getLogger(HallResource.class);

    private static final String ENTITY_NAME = "hall";

    private HallService hallService;

    @Autowired
    public HallResource(HallService hallService) {
        this.hallService = hallService;
    }

    /**
     * It create new Hall Template which will be used to create new screening
     *
     * @param hallTemplateToCreate object of hall which should reflect the real hall in the cinema
     * @return the ResponseEntity with status 201 (Created) or 400 (BadRequest) if parameters had wrong form
     */
    @PostMapping("/hall/template")
    public ResponseEntity<Hall> createHallTemplate(@Valid @RequestBody HallDTO hallTemplateToCreate){
        log.debug("REST request to create new hall template");
        Hall hall = hallService.saveNewTemplate(hallTemplateToCreate);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(hall.getId()).toUri();
        return ResponseEntity.created(location)
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, hall.getId().toString()))
            .body(hall);
    }
}
