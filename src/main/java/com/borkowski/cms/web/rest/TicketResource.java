package com.borkowski.cms.web.rest;

import com.borkowski.cms.domain.Ticket;
import com.borkowski.cms.security.AuthoritiesConstants;
import com.borkowski.cms.service.TicketService;
import com.borkowski.cms.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api")
public class TicketResource {

    private static final String ENTITY_NAME = "ticket";

    private final Logger log = LoggerFactory.getLogger(TicketResource.class);

    private TicketService ticketService;

    @Autowired
    public TicketResource(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    /**
     * It create new type of ticket
     *
     * @param ticket new ticket to save
     * @return The RepsonseEntity with status 201 (Created) and new Ticket in body, or 400 (BadRequest) if id
     */
    @PostMapping("/ticket")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Ticket> createTicket(@Valid @RequestBody Ticket ticket) {
        log.debug("REST request to create new ticket");
        Ticket createdTicket = ticketService.save(ticket);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(createdTicket.getId()).toUri();
        return ResponseEntity.created(location)
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, createdTicket.getId().toString()))
            .body(createdTicket);
    }




}
