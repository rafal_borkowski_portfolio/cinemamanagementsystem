package com.borkowski.cms.web.rest;

import com.borkowski.cms.domain.Movie;
import com.borkowski.cms.security.AuthoritiesConstants;
import com.borkowski.cms.service.MovieService;
import com.borkowski.cms.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api")
@Secured(AuthoritiesConstants.ADMIN)
public class MovieResource {

    private static final String ENTITY_NAME = "movie";

    private final Logger log = LoggerFactory.getLogger(MovieResource.class);

    private MovieService movieService;

    public MovieResource() {
    }

    @Autowired
    public MovieResource(MovieService movieService) {
        this.movieService = movieService;
    }

    /**
     * Create new movie in database
     *
     * @param movie new object of movie to save in database
     * @return the ResponseEntity with status 201 (Created) and the saved movie in body, or status 401 (BadRequest) if id is set
     */
    @PostMapping("/movie")
    public ResponseEntity<Movie> createMovie(@Valid @RequestBody Movie movie){
        log.debug("REST request to create new movie");
        Movie savedMovie = movieService.save(movie);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedMovie.getId()).toUri();
        return ResponseEntity.created(location)
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, savedMovie.getId().toString()))
            .body(savedMovie);
    }

    /**
     * Find movie with specific id
     *
     * @param id the id of movie to find
     * @return the Entity with status 200 (OK) and searched movie in body, or status 404 (Not Found)
     */
    @GetMapping("/movie/{id}")
    public ResponseEntity<Movie> findMovie(@PathVariable Long id) {
        log.debug("REST request to find movie with id: {}", id);
        return ResponseUtil.wrapOrNotFound(movieService.findById(id));
    }

    /**
     * Update whole object of movie
     *
     * @param movieToUpdate movie to update
     * @return the ResponseEntity with status 200 (OK) and no content in body, or status 404 (BadRequest)
     */
    @PutMapping("/movie")
    public ResponseEntity<Movie> updateMovie(@RequestBody Movie movieToUpdate) {
        log.debug("REST request to update movie with id: {}", movieToUpdate.getId());
        if(movieService.findById(movieToUpdate.getId()).isPresent()) {
            movieService.update(movieToUpdate);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
