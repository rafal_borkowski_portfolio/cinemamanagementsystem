package com.borkowski.cms.web.rest;


import com.borkowski.cms.domain.Screening;
import com.borkowski.cms.security.AuthoritiesConstants;
import com.borkowski.cms.service.HallService;
import com.borkowski.cms.service.MovieService;
import com.borkowski.cms.service.ScreeningService;
import com.borkowski.cms.service.dto.ScreeningCreationDTO;
import com.borkowski.cms.service.dto.ScreeningCriteria;
import com.borkowski.cms.service.dto.ScreeningDTO;
import com.borkowski.cms.web.rest.errors.ErrorConstants;
import com.borkowski.cms.web.rest.util.HeaderUtil;
import com.borkowski.cms.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ScreeningResource {

    private static final String ENTITY_NAME = "screening";

    private final Logger log = LoggerFactory.getLogger(ScreeningResource.class);

    private ScreeningService screeningService;

    private MovieService movieService;

    private HallService hallService;

    @Autowired
    public ScreeningResource(ScreeningService screeningService, MovieService movieService, HallService hallService) {
        this.screeningService = screeningService;
        this.movieService = movieService;
        this.hallService = hallService;
    }

    /**
     * Create new Screening object
     *
     * @param screening object of new screening to create
     * @return the ResponseEntity with status 201 (Created) and new Screening object in body,
     * or status 404 (BadRequest) if data to creating new screening will be not sufficient
     */
    @PostMapping("/screening")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Screening> createScreening(@Valid @RequestBody ScreeningCreationDTO screening) {
        log.debug("REST request to create new screening");
        Screening createdScreening = screeningService.save(screening);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(createdScreening.getId()).toUri();
        return ResponseEntity.created(location)
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, createdScreening.getId().toString()))
            .body(createdScreening);
    }

    /**
     * Find list of screening with paging and specific criteria
     *
     * @param criteria criteria of searching
     * @param pageable paging setup
     * @return the ResponseEntity with status 200 (Success) and the Page with list of searched screenings in body
     */
    @GetMapping("/screening")
    public ResponseEntity<List<ScreeningDTO>> getAllScreening(ScreeningCriteria criteria, Pageable pageable) {
        log.debug("REST request to get list of screening");
        final Page<ScreeningDTO> page = screeningService.getAllScreening(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/screening");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * Find specific screening and all information about reserved seats on it
     *
     * @param id the id of searched screening
     * @return the ResponseEntity with status 200 (Success) and the object with searched screening in body
     */
    @GetMapping("/screening/{id}")
    public ResponseEntity<Screening> findScreening(@PathVariable Long id) {
        log.debug("REST request to find all info about screening with id {}", id);
        Screening screening = screeningService.getAllInfoById(id);
        return ResponseEntity.ok().body(screening);
    }

}
