package com.borkowski.cms.web.rest;

import com.borkowski.cms.domain.Reservation;
import com.borkowski.cms.service.ReservationService;
import com.borkowski.cms.service.dto.ReservationDTO;
import com.borkowski.cms.service.dto.ReservationInfoDTO;
import com.borkowski.cms.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api")
public class ReservationResource {

    private final Logger log = LoggerFactory.getLogger(ReservationResource.class);

    private static final String ENTITY_NAME = "reservation";

    private ReservationService reservationService;

    @Autowired
    public ReservationResource(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    /**
     * Create new reservation based on ReservationDTO
     *
     * @param reservationDTO object with required data to create new reservation
     * @return the ResponseEntity with status 201 (Created) and new reservation object in body,
     * or status 404 (BadRequest) if data to creating new reservation will be not sufficient
     */
    @PostMapping("/reservation")
    public ResponseEntity<Reservation> createReservation(@Valid @RequestBody ReservationDTO reservationDTO){
        log.debug("REST request to create new Reservation");
        Reservation reservation = reservationService.createReservation(reservationDTO);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(reservation.getId()).toUri();
        return ResponseEntity.created(location)
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, reservation.getId().toString()))
            .body(reservation);
    }

    /**
     * Find all information about reservation with passed id
     *
     * @param id the id of searched reservation
     * @return the ResponseEntity with status 200 (Success) and with searched reservation object in body,
     * or status 404 (BadRequest) if reservation doesn't exist
     */
    @GetMapping("/reservation/{id}")
    public ResponseEntity<ReservationInfoDTO> findReservationInfo(@PathVariable Long id) {
        log.debug("REST request to find Reservation with id: {}", id);
        ReservationInfoDTO reservation = reservationService.findReservationInfoById(id);
        return ResponseEntity.ok(reservation);
    }

}
