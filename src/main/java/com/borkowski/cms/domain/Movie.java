package com.borkowski.cms.domain;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Entity
@Table(name = "movie")
public class Movie {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Title must be defined")
    @Length(max = 255, message = "Title can has maximum 255 signs")
    private String title;

    @Length(max = 255, message = "Description can has maximum 255 signs")
    private String description;

    public Movie() {
    }

    private Movie(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        setDescription(builder.description);
    }

    public Movie(Movie movie){
        this.id = movie.id;
        this.title = movie.title;
        this.description = movie.description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static final class Builder {
        private Long id;
        private @NotBlank String title;
        private String description;

        public Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder title(@NotBlank String val) {
            title = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Movie build() {
            return new Movie(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Objects.equals(id, movie.id) &&
            Objects.equals(title, movie.title) &&
            Objects.equals(description, movie.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description);
    }
}
