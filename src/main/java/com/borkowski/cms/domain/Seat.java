package com.borkowski.cms.domain;

import com.borkowski.cms.service.dto.SeatDTO;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "seat")
public class Seat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "row_id", referencedColumnName = "id", nullable = false)
    private HallRow row;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reservation_id", referencedColumnName = "id", nullable = false)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Reservation reservation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ticket_id", referencedColumnName = "id", nullable = false)
    private Ticket ticket;

    @NotNull(message = "Seat must have defined column")
    private Integer columnNumber;

    public Seat() {
    }

    public Seat(Integer columnNumber) {
        this.columnNumber = columnNumber;
    }

    public Seat(SeatDTO seatDTO) {
        this.columnNumber = seatDTO.getColumnNumber();
        this.row = new HallRow.Builder().id(seatDTO.getRowId()).build();
        this.ticket = new Ticket.Builder().id(seatDTO.getTicketId()).build();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HallRow getRow() {
        return row;
    }

    public void setRow(HallRow row) {
        this.row = row;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Integer getColumnNumber() {
        return columnNumber;
    }

    public void setColumnNumber(Integer columnNumber) {
        this.columnNumber = columnNumber;
    }
}
