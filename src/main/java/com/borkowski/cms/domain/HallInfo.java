package com.borkowski.cms.domain;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "hall_info")
public class HallInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Name of hall cannot be empty")
    private String name;

    public HallInfo() {
    }

    private HallInfo(Builder builder) {
        setId(builder.id);
        setName(builder.name);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public static final class Builder {
        private Long id;
        private @NotBlank(message = "Name of hall cannot be empty") String name;

        public Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder name(@NotBlank(message = "Name of hall cannot be empty") String val) {
            name = val;
            return this;
        }

        public HallInfo build() {
            return new HallInfo(this);
        }
    }
}
