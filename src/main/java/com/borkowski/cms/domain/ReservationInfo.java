package com.borkowski.cms.domain;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.Instant;

@Entity
@Table(name = "reservation_view")
public class ReservationInfo {

    @Id
    @Column(name = "reservation_id")
    private Long id;

    @NotEmpty(message = "Name may not be empty")
    @Size(min = 3, max = 50, message = "Name must be between 3 and 50 characters long")
    @Pattern(regexp = "^[\\p{Lu}][\\p{Ll}]{2,}$", message = "Name must be started with capital letter")
    private String name;

    @NotEmpty(message = "Surname may not be empty")
    @Size(min = 3, max = 50, message = "Surname must be between 3 and 50 characters long")
    @Pattern(regexp = "([\\p{Lu}][\\p{Ll}]{2,}|[\\p{Lu}][\\p{Ll}]*-[\\p{Lu}][\\p{Ll}]*)",
        message = "Surname must be started with capital letter and if surname " +
            "is made by two parts, each parts also must be started with capital letter")
    private String surname;

    @Embedded
    private SeatInfo seatInfos;

    @NotBlank(message = "Title must be defined")
    @Length(max = 255, message = "Title can has maximum 255 signs")
    @Column(name = "title")
    private String movieTitle;

    @NotNull(message = "Date of play must be defined")
    private Instant playDate;

    public ReservationInfo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public SeatInfo getSeatInfos() {
        return seatInfos;
    }

    public void setSeatInfos(SeatInfo seatInfos) {
        this.seatInfos = seatInfos;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public Instant getPlayDate() {
        return playDate;
    }

    public void setPlayDate(Instant playDate) {
        this.playDate = playDate;
    }
}
