package com.borkowski.cms.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Embeddable
public class SeatInfo {

    @NotNull(message = "Seat must have defined column")
    private Integer columnNumber;

    @Min(value = 1, message = "Hall's number must be positive")
    private Integer rowNumber;

    @NotBlank(message = "Name of hall cannot be empty")
    private String hallName;

    @NotBlank(message = "Name of ticket must be defined")
    private String ticketName;

    @NotNull(message = "Price for ticket must be defined")
    @Column(precision = 5, scale = 2)
    private BigDecimal price;

    public SeatInfo() {
    }

    public Integer getColumnNumber() {
        return columnNumber;
    }

    public void setColumnNumber(Integer columnNumber) {
        this.columnNumber = columnNumber;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public String getTicketName() {
        return ticketName;
    }

    public void setTicketName(String ticketName) {
        this.ticketName = ticketName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
