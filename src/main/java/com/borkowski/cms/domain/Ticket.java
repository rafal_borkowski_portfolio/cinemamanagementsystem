package com.borkowski.cms.domain;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "ticket")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Name of ticket must be defined")
    private String name;

    @NotNull(message = "Price for ticket must be defined")
    @Column(precision = 5, scale = 2)
    private BigDecimal price;

    public Ticket() {
    }

    private Ticket(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setPrice(builder.price);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }


    public static final class Builder {
        private Long id;
        private @NotBlank(message = "Name of ticket must be defined") String name;
        private @NotNull(message = "Price for ticket must be defined") BigDecimal price;

        public Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder name(@NotBlank(message = "Name of ticket must be defined") String val) {
            name = val;
            return this;
        }

        public Builder price(@NotNull(message = "Price for ticket must be defined") BigDecimal val) {
            price = val;
            return this;
        }

        public Ticket build() {
            return new Ticket(this);
        }
    }
}
