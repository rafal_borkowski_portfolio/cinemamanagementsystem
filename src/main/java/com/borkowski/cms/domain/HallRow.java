package com.borkowski.cms.domain;

import com.borkowski.cms.service.dto.HallRowDTO;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.List;

@Entity
@Table(name = "hall_row")
public class HallRow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Min(value = 1, message = "Row in hall must have minimum 1 seat")
    private Integer numberOfSeats;

    @JsonBackReference
    @ManyToOne(optional = false)
    @JoinColumn(name = "hall_id")
    private Hall hall;

    @JsonManagedReference
    @OneToMany(mappedBy = "row", fetch = FetchType.EAGER)
    private List<Seat> seats;

    @Min(value = 1, message = "Hall's number must be positive")
    private Integer rowNumber;

    public HallRow() {
    }

    public HallRow (HallRow rowTemplate) {
        this.hall = rowTemplate.getHall();
        this.numberOfSeats = rowTemplate.getNumberOfSeats();
        this.rowNumber = rowTemplate.getRowNumber();
    }

    public HallRow (HallRowDTO rowTemplate) {
        this.numberOfSeats = rowTemplate.getNumberOfSeats();
    }

    private HallRow(Builder builder) {
        setId(builder.id);
        setNumberOfSeats(builder.numberOfSeats);
        setHall(builder.hall);
        setSeats(builder.seats);
        setRowNumber(builder.rowNumber);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }

    public Integer getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(Integer numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }


    public static final class Builder {
        private Long id;
        private @Min(value = 1, message = "Row in hall must have minimum 1 seat") Integer numberOfSeats;
        private Hall hall;
        private List<Seat> seats;
        private @Min(value = 1, message = "Hall's number must be positive") Integer rowNumber;

        public Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder numberOfSeats(@Min(value = 1, message = "Row in hall must have minimum 1 seat") Integer val) {
            numberOfSeats = val;
            return this;
        }

        public Builder hall(Hall val) {
            hall = val;
            return this;
        }

        public Builder seats(List<Seat> val) {
            seats = val;
            return this;
        }

        public Builder rowNumber(@Min(value = 1, message = "Hall's number must be positive") Integer val) {
            rowNumber = val;
            return this;
        }

        public HallRow build() {
            return new HallRow(this);
        }
    }
}
