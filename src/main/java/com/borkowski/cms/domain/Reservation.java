package com.borkowski.cms.domain;

import com.borkowski.cms.service.dto.ReservationDTO;
import com.borkowski.cms.service.dto.SeatDTO;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "reservation")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Name may not be empty")
    @Size(min = 3, max = 50, message = "Name must be between 3 and 50 characters long")
    @Pattern(regexp = "^[\\p{Lu}][\\p{Ll}]{2,}$", message = "Name must be started with capital letter")
    private String name;

    @NotEmpty(message = "Surname may not be empty")
    @Size(min = 3, max = 50, message = "Surname must be between 3 and 50 characters long")
    @Pattern(regexp = "([\\p{Lu}][\\p{Ll}]{2,}|[\\p{Lu}][\\p{Ll}]*-[\\p{Lu}][\\p{Ll}]*)",
        message = "Surname must be started with capital letter and if surname " +
            "is made by two parts, each parts also must be started with capital letter")
    private String surname;

    @JsonManagedReference
    @OneToMany(mappedBy = "reservation", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Seat> seats;

    public Reservation() {
    }

    public Reservation(ReservationDTO reservationDTO) {
        this.name = reservationDTO.getName();
        this.surname = reservationDTO.getSurname();
        List<Seat> seats = new ArrayList<>();
        for(SeatDTO seat: reservationDTO.getSeats()){
            Seat newSeat = new Seat(seat);
            newSeat.setReservation(this);
            seats.add(newSeat);
        }
        this.seats = seats;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }
}
