package com.borkowski.cms.domain;

import com.borkowski.cms.service.dto.HallDTO;
import com.borkowski.cms.service.dto.HallRowDTO;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "hall")
public class Hall {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "hall_info_id", referencedColumnName = "id", nullable = false)
    private HallInfo hallInfo;

    @JsonManagedReference
    @Size(min = 1, message = "Hall must have at least 1 row")
    @OneToMany(mappedBy = "hall", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private List<HallRow> rows;

    private Boolean isTemplate;

    public Hall() {
    }

    public Hall(Hall templateHall) {
        List<HallRow> rows = new ArrayList<>();
        for(HallRow row: templateHall.getRows()) {
            HallRow newRow = new HallRow(row);
            newRow.setHall(this);
            rows.add(newRow);
        }
        this.rows = rows;
        this.hallInfo = templateHall.getHallInfo();
        this.isTemplate = false;
    }

    public Hall(HallDTO hallDTO) {
        this.hallInfo = hallDTO.getHallInfo();
        this.rows = new ArrayList<>();
        int counter = 1;
        for(HallRowDTO row: hallDTO.getRows()){
            HallRow newRow = new HallRow(row);
            newRow.setHall(this);
            newRow.setRowNumber(counter++);
            this.rows.add(newRow);
        }
    }

    private Hall(Builder builder) {
        setId(builder.id);
        setHallInfo(builder.hallInfo);
        setRows(builder.rows);
        isTemplate = builder.isTemplate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HallInfo getHallInfo() {
        return hallInfo;
    }

    public void setHallInfo(HallInfo hallInfo) {
        this.hallInfo = hallInfo;
    }

    public List<HallRow> getRows() {
        return rows;
    }

    public void setRows(List<HallRow> rows) {
        this.rows = rows;
    }

    public Boolean getTemplate() {
        return isTemplate;
    }

    public void setTemplate(Boolean template) {
        isTemplate = template;
    }


    public static final class Builder {
        private Long id;
        private HallInfo hallInfo;
        private @Size(min = 1, message = "Hall must have at least 1 row") List<HallRow> rows;
        private Boolean isTemplate;

        public Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder hallInfo(HallInfo val) {
            hallInfo = val;
            return this;
        }

        public Builder rows(@Size(min = 1, message = "Hall must have at least 1 row") List<HallRow> val) {
            rows = val;
            return this;
        }

        public Builder isTemplate(Boolean val) {
            isTemplate = val;
            return this;
        }

        public Hall build() {
            return new Hall(this);
        }
    }
}
