package com.borkowski.cms.service;

import com.borkowski.cms.domain.Movie;

import java.util.Optional;

public interface MovieService {

    /**
     * Save new movie in database
     *
     * @param movie - object to save
     * @return new saved object of movie
     */
    Movie save(Movie movie);

    /**
     * Find movie with passed id
     *
     * @param id the id of movie which should be find
     * @return optional with searched object of movie
     */
    Optional<Movie> findById(Long id);

    /**
     * Update movie object in database
     *
     * @param movieToUpdate updated movie object to save
     * @return updated movie object
     */
    Movie update(Movie movieToUpdate);
}
