package com.borkowski.cms.service;

import com.borkowski.cms.domain.Screening;
import com.borkowski.cms.service.dto.ScreeningCreationDTO;
import com.borkowski.cms.service.dto.ScreeningCriteria;
import com.borkowski.cms.service.dto.ScreeningDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ScreeningService {

    /**
     * Save new screening
     *
     * @param screening screening object to save
     * @return created object of screening with new unique ID
     */
    Screening save(ScreeningCreationDTO screening);

    /**
     * @see ScreeningQueryService
     */
    Page<ScreeningDTO> getAllScreening(ScreeningCriteria criteria, Pageable pageable) ;

    /**
     * Find all information about reserved seats on specific screening
     *
     * @param id the id of searched screening
     * @return object of screening with all information about reserved seats
     */
    Screening getAllInfoById(Long id);
}
