package com.borkowski.cms.service;

import com.borkowski.cms.domain.Hall;
import com.borkowski.cms.service.dto.HallDTO;

import java.util.Optional;

public interface HallService {

    /**
     * Save new template of Hall which will be used to create halls for screening
     *
     * @param hallTemplateToCreate object which describe new template of hall
     * @return new Object of Hall
     */
    Hall saveNewTemplate(HallDTO hallTemplateToCreate);

    /**
     * Find Hall with passed id
     *
     * @param id the id of searched hall
     * @return Optional object with potential hall
     */
    Optional<Hall> findById(Long id);


    /**
     * Find hall template for creating new hall during creation new screening
     *
     * @param id the id of searched hall template
     * @return Optional with searched hall template
     */
    Optional<Hall> findByIdAndTemplateTrue(Long id);
}
