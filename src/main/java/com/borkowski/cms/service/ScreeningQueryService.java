package com.borkowski.cms.service;

import com.borkowski.cms.domain.Screening;
import com.borkowski.cms.domain.Screening_;
import com.borkowski.cms.repository.ScreeningRepository;
import com.borkowski.cms.service.dto.ScreeningCriteria;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ScreeningQueryService extends QueryService<Screening> {

    private final Logger log = LoggerFactory.getLogger(ScreeningQueryService.class);

    private ScreeningRepository screeningRepository;

    @Autowired
    public ScreeningQueryService(ScreeningRepository screeningRepository) {
        this.screeningRepository = screeningRepository;
    }

    /**
     * It allows to search screening object as the pageable object with particular criteria of looking
     *
     * @param criteria criteria of searching
     * @param pageable setup about paging
     * @return Page with searched list of screening
     */
    public Page<Screening> findByCriteria(ScreeningCriteria criteria, Pageable pageable) {
        log.debug("find screening by criteria: {}", criteria);
        final Specification<Screening> specification = createSpecification(criteria);
        return screeningRepository.findAll(specification, pageable);
    }

    private Specification<Screening> createSpecification(ScreeningCriteria criteria) {
        Specification<Screening> specification = Specification.where(null);
        if(criteria != null){
            if(criteria.getPlayDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPlayDate(), Screening_.playDate));
            }
        }
        return specification;
    }

}
