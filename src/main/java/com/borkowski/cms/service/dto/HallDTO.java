package com.borkowski.cms.service.dto;

import com.borkowski.cms.domain.HallInfo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class HallDTO {

    @NotNull(message = "Hall must have declared info about itself")
    private HallInfo hallInfo;

    @Size(min = 1, message = "Hall must have at least 1 row")
    private List<HallRowDTO> rows;

    public HallDTO() {
    }

    private HallDTO(Builder builder) {
        setHallInfo(builder.hallInfo);
        setRows(builder.rows);
    }

    public HallInfo getHallInfo() {
        return hallInfo;
    }

    public void setHallInfo(HallInfo hallInfo) {
        this.hallInfo = hallInfo;
    }

    public List<HallRowDTO> getRows() {
        return rows;
    }

    public void setRows(List<HallRowDTO> rows) {
        this.rows = rows;
    }


    public static final class Builder {
        private @NotNull(message = "Hall must have declared info about itself") HallInfo hallInfo;
        private @Size(min = 1, message = "Hall must have at least 1 row") List<HallRowDTO> rows;

        public Builder() {
        }

        public Builder hallInfo(@NotNull(message = "Hall must have declared info about itself") HallInfo val) {
            hallInfo = val;
            return this;
        }

        public Builder rows(@Size(min = 1, message = "Hall must have at least 1 row") List<HallRowDTO> val) {
            rows = val;
            return this;
        }

        public HallDTO build() {
            return new HallDTO(this);
        }
    }
}
