package com.borkowski.cms.service.dto;

import com.borkowski.cms.domain.Movie;
import com.borkowski.cms.domain.Screening;

import javax.validation.constraints.NotNull;
import java.time.Instant;

public class ScreeningDTO {

    private Long id;

    @NotNull(message = "Movie must be assigned")
    private Movie movie;

    @NotNull(message = "Date of play must be defined")
    private Instant playDate;

    public ScreeningDTO(Screening screening) {
        this.id = screening.getId();
        this.movie = screening.getMovie();
        this.playDate = screening.getPlayDate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Instant getPlayDate() {
        return playDate;
    }

    public void setPlayDate(Instant playDate) {
        this.playDate = playDate;
    }
}
