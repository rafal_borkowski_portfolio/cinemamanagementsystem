package com.borkowski.cms.service.dto;

import javax.validation.constraints.NotNull;
import java.time.Instant;

public class ScreeningCreationDTO {

    @NotNull(message = "Movie id must be defined")
    private Long movieId;

    @NotNull(message = "Hall id must be defined")
    private Long hallId;

    @NotNull(message = "Date when movie will be played must be defined")
    private Instant playDate;

    public ScreeningCreationDTO() {
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public Long getHallId() {
        return hallId;
    }

    public void setHallId(Long hallId) {
        this.hallId = hallId;
    }

    public Instant getPlayDate() {
        return playDate;
    }

    public void setPlayDate(Instant playDate) {
        this.playDate = playDate;
    }
}
