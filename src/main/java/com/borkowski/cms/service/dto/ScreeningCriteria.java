package com.borkowski.cms.service.dto;

import io.github.jhipster.service.filter.InstantFilter;

import java.io.Serializable;

public class ScreeningCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private InstantFilter playDate;

    public InstantFilter getPlayDate() {
        return playDate;
    }

    public void setPlayDate(InstantFilter playDate) {
        this.playDate = playDate;
    }
}
