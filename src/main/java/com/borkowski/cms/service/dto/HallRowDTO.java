package com.borkowski.cms.service.dto;

import javax.validation.constraints.Min;

public class HallRowDTO {

    @Min(value = 1, message = "Row in hall must have minimum 1 seat")
    private Integer numberOfSeats;

    public HallRowDTO() {
    }

    private HallRowDTO(Builder builder) {
        setNumberOfSeats(builder.numberOfSeats);
    }

    public Integer getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(Integer numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }


    public static final class Builder {
        private @Min(value = 1, message = "Row in hall must have minimum 1 seat") Integer numberOfSeats;

        public Builder() {
        }

        public Builder numberOfSeats(@Min(value = 1, message = "Row in hall must have minimum 1 seat") Integer val) {
            numberOfSeats = val;
            return this;
        }

        public HallRowDTO build() {
            return new HallRowDTO(this);
        }
    }
}
