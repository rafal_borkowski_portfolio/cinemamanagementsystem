package com.borkowski.cms.service.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class SeatDTO {

    @NotNull(message = "Hall row id must be declared")
    private Long rowId;

    @Min(value = 1, message = "Column number must be positive")
    private Integer columnNumber;

    @NotNull(message = "Ticket id must be declared")
    private Long ticketId;

    public SeatDTO() {
    }

    public Long getRowId() {
        return rowId;
    }

    public void setRowId(Long rowId) {
        this.rowId = rowId;
    }

    public Integer getColumnNumber() {
        return columnNumber;
    }

    public void setColumnNumber(Integer columnNumber) {
        this.columnNumber = columnNumber;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }
}
