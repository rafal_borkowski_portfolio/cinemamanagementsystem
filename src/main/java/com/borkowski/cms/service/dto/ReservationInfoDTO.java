package com.borkowski.cms.service.dto;

import com.borkowski.cms.domain.ReservationInfo;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Id;
import javax.validation.constraints.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class ReservationInfoDTO {

    @Id
    private Long id;

    @NotEmpty(message = "Name may not be empty")
    @Size(min = 3, max = 50, message = "Name must be between 3 and 50 characters long")
    @Pattern(regexp = "^[\\p{Lu}][\\p{Ll}]{2,}$", message = "Name must be started with capital letter")
    private String name;

    @NotEmpty(message = "Surname may not be empty")
    @Size(min = 3, max = 50, message = "Surname must be between 3 and 50 characters long")
    @Pattern(regexp = "([\\p{Lu}][\\p{Ll}]{2,}|[\\p{Lu}][\\p{Ll}]*-[\\p{Lu}][\\p{Ll}]*)",
        message = "Surname must be started with capital letter and if surname " +
            "is made by two parts, each parts also must be started with capital letter")
    private String surname;

    private List<SeatInfoDTO> seatInfos;

    @NotBlank(message = "Title must be defined")
    @Length(max = 255, message = "Title can has maximum 255 signs")
    private String movieTitle;

    @NotNull(message = "Date of play must be defined")
    private Instant playDate;

    public ReservationInfoDTO(List<ReservationInfo> reservationInfo) {
        if(reservationInfo.size()!= 0) {
            this.id = reservationInfo.get(0).getId();
            this.movieTitle = reservationInfo.get(0).getMovieTitle();
            this.name = reservationInfo.get(0).getName();
            this.surname = reservationInfo.get(0).getSurname();
            this.playDate = reservationInfo.get(0).getPlayDate();
            this.seatInfos = new ArrayList<>();
            for(ReservationInfo res: reservationInfo) {
                this.seatInfos.add(new SeatInfoDTO(res.getSeatInfos()));
            }
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<SeatInfoDTO> getSeatInfos() {
        return seatInfos;
    }

    public void setSeatInfos(List<SeatInfoDTO> seatInfos) {
        this.seatInfos = seatInfos;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public Instant getPlayDate() {
        return playDate;
    }

    public void setPlayDate(Instant playDate) {
        this.playDate = playDate;
    }
}
