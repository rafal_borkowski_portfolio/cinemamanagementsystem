package com.borkowski.cms.service.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

public class ReservationDTO {

    @NotEmpty(message = "Name may not be empty")
    @Size(min = 3, max = 50, message = "Name must be between 3 and 50 characters long")
    @Pattern(regexp = "^[\\p{Lu}][\\p{Ll}]{2,}$", message = "Name must be started with capital letter")
    private String name;

    @NotEmpty(message = "Surname may not be empty")
    @Size(min = 3, max = 50, message = "Surname must be between 3 and 50 characters long")
    @Pattern(regexp = "([\\p{Lu}][\\p{Ll}]{2,}|[\\p{Lu}][\\p{Ll}]*-[\\p{Lu}][\\p{Ll}]*)",
        message = "Surname must be started with capital letter and if surname " +
            "is made by two parts, each parts also must be started with capital letter")
    private String surname;

    @Size(min = 1, message = "Number of seats must be at least 1")
    private List<SeatDTO> seats;

    public ReservationDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<SeatDTO> getSeats() {
        return seats;
    }

    public void setSeats(List<SeatDTO> seats) {
        this.seats = seats;
    }
}
