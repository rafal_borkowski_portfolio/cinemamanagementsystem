package com.borkowski.cms.service.impl;

import com.borkowski.cms.domain.Ticket;
import com.borkowski.cms.repository.TicketRepository;
import com.borkowski.cms.service.TicketService;
import com.borkowski.cms.web.rest.errors.ErrorConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TicketServiceImpl implements TicketService {

    private final Logger log = LoggerFactory.getLogger(TicketServiceImpl.class);

    private TicketRepository ticketRepository;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Override
    public Ticket save(Ticket ticket) {
        log.debug("Request to save new Ticket object");
        if(ticket.getId() != null) {
            throw new IllegalArgumentException(ErrorConstants.ID_IS_NOT_NULL);
        }
        return ticketRepository.save(ticket);
    }
}
