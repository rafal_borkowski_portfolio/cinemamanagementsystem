package com.borkowski.cms.service.impl;

import com.borkowski.cms.domain.Movie;
import com.borkowski.cms.repository.MovieRepository;
import com.borkowski.cms.service.MovieService;
import com.borkowski.cms.web.rest.errors.ErrorConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class MovieServiceImpl implements MovieService {

    private final Logger log = LoggerFactory.getLogger(MovieServiceImpl.class);

    private MovieRepository movieRepository;

    @Autowired
    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }


    @Override
    public Movie save(Movie movie) {
        log.debug("Request to save new Movie object");
        if(movie.getId() != null) {
            throw new IllegalArgumentException(ErrorConstants.ID_IS_NOT_NULL);
        }
        return movieRepository.save(movie);
    }

    @Override
    public Optional<Movie> findById(Long id) {
        log.debug("Request to find movie with id: {}", id);
        if(id == null) {
            throw new IllegalArgumentException(ErrorConstants.ID_IS_NULL);
        } else if(id < 0) {
            throw new IllegalArgumentException(ErrorConstants.ID_IS_NEGATIVE);
        }
        return movieRepository.findById(id);
    }

    @Override
    public Movie update(Movie movieToUpdate) {
        log.debug("Request to update movie with id: {}", movieToUpdate.getId());
        if(movieToUpdate.getId() == null) {
            throw new IllegalArgumentException(ErrorConstants.ID_IS_NULL);
        }else if(movieToUpdate.getId() < 0) {
            throw new IllegalArgumentException(ErrorConstants.ID_IS_NEGATIVE);
        }
        return movieRepository.save(movieToUpdate);
    }
}
