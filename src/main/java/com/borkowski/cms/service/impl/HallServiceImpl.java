package com.borkowski.cms.service.impl;

import com.borkowski.cms.domain.Hall;
import com.borkowski.cms.repository.HallRepository;
import com.borkowski.cms.service.HallService;
import com.borkowski.cms.service.dto.HallDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class HallServiceImpl implements HallService {

    private final Logger log = LoggerFactory.getLogger(HallServiceImpl.class);

    private HallRepository hallRepository;

    @Autowired
    public HallServiceImpl(HallRepository hallRepository) {
        this.hallRepository = hallRepository;
    }

    @Override
    public Hall saveNewTemplate(HallDTO hallTemplateToCreate) {
        log.debug("Request to save new Hall Template object");
        Hall newHall = new Hall(hallTemplateToCreate);
        newHall.setTemplate(true);
        return hallRepository.save(newHall);
    }

    @Override
    public Optional<Hall> findById(Long id) {
        log.debug("Request to find Hall with id {}", id);
        if(id == null) {
            throw new IllegalArgumentException("Id must be defined");
        }
        return hallRepository.findById(id);
    }

    @Override
    public Optional<Hall> findByIdAndTemplateTrue(Long id) {
        log.debug("Request to find Hall Template with id {}", id);
        if(id == null) {
            throw new IllegalArgumentException("Id must be defined");
        }
        return hallRepository.findByIdAndIsTemplateTrue(id);
    }
}
