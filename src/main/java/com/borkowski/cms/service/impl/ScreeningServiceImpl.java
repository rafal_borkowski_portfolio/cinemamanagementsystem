package com.borkowski.cms.service.impl;

import com.borkowski.cms.domain.Hall;
import com.borkowski.cms.domain.Screening;
import com.borkowski.cms.repository.ScreeningRepository;
import com.borkowski.cms.service.HallService;
import com.borkowski.cms.service.MovieService;
import com.borkowski.cms.service.ScreeningQueryService;
import com.borkowski.cms.service.ScreeningService;
import com.borkowski.cms.service.dto.ScreeningCreationDTO;
import com.borkowski.cms.service.dto.ScreeningCriteria;
import com.borkowski.cms.service.dto.ScreeningDTO;
import com.borkowski.cms.web.rest.errors.ErrorConstants;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ScreeningServiceImpl implements ScreeningService {

    private final Logger log = LoggerFactory.getLogger(ScreeningServiceImpl.class);

    private ScreeningRepository screeningRepository;

    private ScreeningQueryService screeningQueryService;

    private MovieService movieService;

    private HallService hallService;

    @Autowired
    public ScreeningServiceImpl(ScreeningRepository screeningRepository, ScreeningQueryService screeningQueryService, MovieService movieService, HallService hallService) {
        this.screeningRepository = screeningRepository;
        this.screeningQueryService = screeningQueryService;
        this.movieService = movieService;
        this.hallService = hallService;
    }

    @Override
    public Screening save(ScreeningCreationDTO screening) {
        log.debug("Request to save new Screening");
        Screening newScreening = new Screening();
        newScreening.setPlayDate(screening.getPlayDate());

        newScreening.setMovie(movieService.findById(screening.getMovieId()).orElseThrow(() -> new IllegalArgumentException(ErrorConstants.MOVIE_IS_NOT_EXIST)));

        Hall templateHall = hallService.findByIdAndTemplateTrue(screening.getHallId()).orElseThrow(() -> new IllegalArgumentException(ErrorConstants.HALL_IS_NOT_EXIST));
        Hall newHall = new Hall(templateHall);
        newScreening.setHall(newHall);
        return screeningRepository.save(newScreening);
    }

    @Override
    public Page<ScreeningDTO> getAllScreening(ScreeningCriteria criteria, Pageable pageable) {
        return screeningQueryService.findByCriteria(criteria, pageable).map(ScreeningDTO::new);
    }

    @Override
    public Screening getAllInfoById(Long id) {
        Screening screening = screeningRepository.findById(id).orElseThrow(() -> new IllegalArgumentException(ErrorConstants.SCREENING_WITH_ID_NOT_EXIST));
        Hibernate.initialize(screening.getHall());
        Hibernate.initialize(screening.getHall().getRows());
        return screening;
    }
}
