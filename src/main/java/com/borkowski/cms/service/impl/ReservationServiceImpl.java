package com.borkowski.cms.service.impl;

import com.borkowski.cms.domain.HallRow;
import com.borkowski.cms.domain.Reservation;
import com.borkowski.cms.domain.ReservationInfo;
import com.borkowski.cms.domain.Seat;
import com.borkowski.cms.repository.*;
import com.borkowski.cms.service.ReservationService;
import com.borkowski.cms.service.dto.ReservationDTO;
import com.borkowski.cms.service.dto.ReservationInfoDTO;
import com.borkowski.cms.service.dto.SeatDTO;
import com.borkowski.cms.web.rest.errors.ErrorConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static java.lang.Math.abs;

@Service
@Transactional
public class ReservationServiceImpl implements ReservationService {

    private final Logger log = LoggerFactory.getLogger(ReservationServiceImpl.class);

    private TicketRepository ticketRepository;

    private HallRowRepository hallRowRepository;

    private ScreeningRepository screeningRepository;

    private ReservationRepository reservationRepository;

    private ReservationViewRepository reservationViewRepository;

    @Autowired
    public ReservationServiceImpl(TicketRepository ticketRepository,
                                  HallRowRepository hallRowRepository,
                                  ScreeningRepository screeningRepository,
                                  ReservationRepository reservationRepository,
                                  ReservationViewRepository reservationViewRepository) {
        this.ticketRepository = ticketRepository;
        this.hallRowRepository = hallRowRepository;
        this.screeningRepository = screeningRepository;
        this.reservationRepository = reservationRepository;
        this.reservationViewRepository = reservationViewRepository;
    }

    @Override
    public Reservation createReservation(ReservationDTO reservationDTO) {
        log.debug("Request to create new Reservation");
        HashMap<Long, HallRow> rows = downloadListOfRows(reservationDTO.getSeats());

        validReservedSeats(rows, reservationDTO.getSeats());

        Reservation reservation = new Reservation(reservationDTO);

        return reservationRepository.save(reservation);
    }

    @Override
    public ReservationInfoDTO findReservationInfoById(Long id) {
        log.debug("Request to find Reservation Info by id: {}", id);
        List<ReservationInfo> reservationInfos = reservationViewRepository.findAllById(id);

        ReservationInfoDTO reservationInfoDTO = new ReservationInfoDTO(reservationInfos);
        return reservationInfoDTO;
    }

    private HashMap<Long, HallRow> downloadListOfRows(List<SeatDTO> seats) {
        Set<Long> rowsNumbers = new HashSet<>();
        for(SeatDTO seat: seats){
            rowsNumbers.add(seat.getRowId());
        }
        HashMap<Long, HallRow> rows = new HashMap<>();
        Iterator iterator = rowsNumbers.iterator();
        while(iterator.hasNext()){
            Long id = (Long)iterator.next();
            HallRow row = hallRowRepository.findById(id).orElseThrow(() -> new IllegalArgumentException(ErrorConstants.ROW_WITH_ID_NOT_EXIST));
            rows.put(id, row);
        }
        return rows;
    }

    private void validReservedSeats(HashMap<Long, HallRow> rows, List<SeatDTO> seats) {
        //At first check if seats which client want to reserve is valid and free
        for(SeatDTO seat: seats) {
            HallRow row = rows.get(seat.getRowId());
            if(seat.getColumnNumber() < 1 || seat.getColumnNumber() > row.getNumberOfSeats()) {
                throw new IllegalArgumentException(ErrorConstants.COLUMN_NUMBER_IS_INVALID);
            }
            for(Seat reservedSeat: row.getSeats()) {
                if(reservedSeat.getColumnNumber().equals(seat.getColumnNumber())) {
                    throw new IllegalArgumentException(ErrorConstants.SEAT_IS_RESERVED);
                }
            }
            Timestamp playDate = screeningRepository.findDateOfScreening(seat.getRowId()).orElseThrow(() -> new IllegalArgumentException(ErrorConstants.SCREENING_WITH_ID_NOT_EXIST));
            if(playDate.toInstant().minus(15, ChronoUnit.MINUTES).isBefore(Instant.now())) {
                throw new IllegalArgumentException(ErrorConstants.TIME_IS_UP);
            }
            row.getSeats().add(new Seat(seat.getColumnNumber()));
            rows.put(seat.getRowId(), row);
        }
        //The next step is checking gap between seats
        for(Long key: rows.keySet()) {
            HallRow validatedRow = rows.get(key);
            List<Seat> validatedSeats = validatedRow.getSeats();
            validatedSeats.sort(Comparator.comparing(Seat::getColumnNumber));
            for(int i = 0; i < validatedSeats.size() - 1; i++) {
                if(abs(validatedSeats.get(i).getColumnNumber() - validatedSeats.get(i+1).getColumnNumber()) == 2) {
                    throw new IllegalArgumentException(ErrorConstants.GAP_BETWEEN_SEATS);
                }
            }
        }
    }
}
