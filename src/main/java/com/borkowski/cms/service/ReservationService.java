package com.borkowski.cms.service;

import com.borkowski.cms.domain.Reservation;
import com.borkowski.cms.domain.ReservationInfo;
import com.borkowski.cms.service.dto.ReservationDTO;
import com.borkowski.cms.service.dto.ReservationInfoDTO;

import java.util.Optional;

public interface ReservationService {

    /**
     * Create new Reservation of seats in particular screening
     *
     * @param reservationDTO object with new reservation
     * @return new Reservation object
     */
    Reservation createReservation(ReservationDTO reservationDTO);

    /**
     * Find information about reservation with passed Id
     *
     * That method uses Reservation View in database
     *
     * @param id the id of searched reservation
     * @return info about searched reservation
     */
    //I didn't create alone service for Reservation View because it's only one place where I use it,
    // in other case I would create separate service for it
    ReservationInfoDTO findReservationInfoById(Long id);

}
