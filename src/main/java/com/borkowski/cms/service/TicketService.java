package com.borkowski.cms.service;

import com.borkowski.cms.domain.Ticket;

public interface TicketService {

    /**
     * It save new ticket type
     *
     * @param ticket ticket object to save
     * @return New object of saved ticket
     */
    Ticket save(Ticket ticket);
}
