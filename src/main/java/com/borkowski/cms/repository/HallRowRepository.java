package com.borkowski.cms.repository;

import com.borkowski.cms.domain.HallRow;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HallRowRepository extends CrudRepository<HallRow, Long> {
}
