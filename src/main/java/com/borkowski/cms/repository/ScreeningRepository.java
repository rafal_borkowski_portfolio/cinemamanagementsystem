package com.borkowski.cms.repository;

import com.borkowski.cms.domain.Screening;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;

@Repository
public interface ScreeningRepository extends CrudRepository<Screening, Long>, JpaSpecificationExecutor<Screening> {

    @Query(value = "Select s.play_date from screening s " +
        "left join hall h on s.hall_id = h.id " +
        "left join hall_row hr on hr.hall_id = h.id " +
        "where hr.id = :rowId", nativeQuery = true)
    Optional<Timestamp> findDateOfScreening(@Param("rowId") Long rowId);

}
