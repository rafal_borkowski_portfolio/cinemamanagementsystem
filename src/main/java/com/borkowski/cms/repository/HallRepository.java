package com.borkowski.cms.repository;

import com.borkowski.cms.domain.Hall;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HallRepository extends CrudRepository<Hall, Long> {

    Optional<Hall> findByIdAndIsTemplateTrue(Long id);
}
