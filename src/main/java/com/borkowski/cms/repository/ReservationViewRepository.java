package com.borkowski.cms.repository;

import com.borkowski.cms.domain.ReservationInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationViewRepository extends CrudRepository<ReservationInfo, Long> {

    @Query("select ri from ReservationInfo ri where ri.id = :id")
    List<ReservationInfo> findAllById(@Param("id") Long id);
}
