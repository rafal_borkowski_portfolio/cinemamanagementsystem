package com.borkowski.cms.web.rest;

import com.borkowski.cms.domain.Movie;
import com.borkowski.cms.service.MovieService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {MovieResource.class}, secure = false)
public class MovieResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovieService movieService;

    @Test
    public void saveMovie_saveNormally() throws Exception {
        //Given
        Movie movieToSave = new Movie.Builder()
            .title("My new wonderful movie")
            .description("Some description here")
            .build();
        Movie savedMovie = new Movie(movieToSave);
        savedMovie.setId(1L);
        given(movieService.save(any(Movie.class))).willReturn(savedMovie);
        //When & Then
        mockMvc.perform(post("/api/movie")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(movieToSave)))
            .andDo(print())
            .andExpect(status().isCreated())
            .andExpect(header().string("Location", containsString("/api/movie/" + savedMovie.getId().toString())))
            .andExpect(jsonPath("$.id", is(savedMovie.getId().intValue())))
            .andExpect(jsonPath("$.title", is(savedMovie.getTitle())))
            .andExpect(jsonPath("$.description", is(savedMovie.getDescription())));
    }

    @Test
    public void findMovieById_returnMovieNormally() throws Exception {
        //Given
        Movie movie = new Movie.Builder()
            .id(1L)
            .title("My new wonderful movie")
            .description("Some description here")
            .build();
        given(movieService.findById(movie.getId())).willReturn(Optional.of(movie));
        //When & Then
        mockMvc.perform(get("/api/movie/" + movie.getId().intValue()))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.id", is(movie.getId().intValue())))
            .andExpect(jsonPath("$.title", is(movie.getTitle())))
            .andExpect(jsonPath("$.description", is(movie.getDescription())));
    }

    @Test
    public void findMovieById_movieNotFound() throws Exception {
        //Given
        given(movieService.findById(anyLong())).willReturn(Optional.empty());
        //When & Then
        mockMvc.perform(get("/api/movie/1"))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateMovie_updateMovieNormally() throws Exception {
        //Given
        Movie movieToUpdate = new Movie.Builder()
            .id(1L)
            .title("My new wonderful movie")
            .description("Some description here")
            .build();
        given(movieService.findById(movieToUpdate.getId())).willReturn(Optional.of(movieToUpdate));
        //When & Then
        mockMvc.perform(put("/api/movie")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(movieToUpdate)))
            .andDo(print())
            .andExpect(status().isNoContent());
    }

    @Test
    public void updateMovie_movieNotFound() throws Exception {
        //Given
        Movie movieToUpdate = new Movie.Builder()
            .id(1L)
            .title("My new wonderful movie")
            .description("Some description here")
            .build();
        given(movieService.findById(anyLong())).willReturn(Optional.empty());
        //When & Then
        mockMvc.perform(put("/api/movie")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(movieToUpdate)))
            .andDo(print())
            .andExpect(status().isNotFound());
    }
}
