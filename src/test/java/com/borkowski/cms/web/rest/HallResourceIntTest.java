package com.borkowski.cms.web.rest;

import com.borkowski.cms.CinemaManagementSystemApp;
import com.borkowski.cms.service.HallService;
import com.borkowski.cms.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CinemaManagementSystemApp.class)
public class HallResourceIntTest {

    @Autowired
    private HallService hallService;

    private MockMvc mockMvc;

    @Before
    public void setUp(){
        this.mockMvc = MockMvcBuilders
            .standaloneSetup(new HallResource(hallService))
            .setControllerAdvice(new ExceptionTranslator())
            .build();
    }
}
