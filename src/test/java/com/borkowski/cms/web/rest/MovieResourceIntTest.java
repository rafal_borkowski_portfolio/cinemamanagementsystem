package com.borkowski.cms.web.rest;

import com.borkowski.cms.CinemaManagementSystemApp;
import com.borkowski.cms.domain.Movie;
import com.borkowski.cms.service.MovieService;
import com.borkowski.cms.web.rest.errors.ErrorConstants;
import com.borkowski.cms.web.rest.errors.ExceptionTranslator;
import io.swagger.annotations.Authorization;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CinemaManagementSystemApp.class)
public class MovieResourceIntTest {

    @Autowired
    private MovieService movieService;

    private MockMvc mockMvc;

    @Before
    public void setUp(){
        this.mockMvc = MockMvcBuilders
            .standaloneSetup(new MovieResource(movieService))
            .setControllerAdvice(new ExceptionTranslator())
            .build();
    }

    @Test
    public void saveMovie_idIsNotNull_ThrowException() throws Exception {
        //Given
        Movie movieToSave = new Movie.Builder()
            .id(1L)
            .title("My new wonderful movie")
            .description("Some description here")
            .build();
        //When & Then
        mockMvc.perform(post("/api/movie")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(movieToSave)))
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.title", is(ErrorConstants.ID_IS_NOT_NULL)));
    }

    @Test
    public void findMovie_idIsNegative_ThrowException() throws Exception {
        //When & Then
        mockMvc.perform(get("/api/movie/-1"))
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.title", is(ErrorConstants.ID_IS_NEGATIVE)));
    }

    @Test
    public void updateMovie_idIsNotNull_ThrowException() throws Exception {
        //Given
        Movie movieToUpdate = new Movie.Builder()
            .id(null)
            .title("My new wonderful movie")
            .description("Some description here")
            .build();
        //When & Then
        mockMvc.perform(put("/api/movie")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(movieToUpdate)))
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.title", is(ErrorConstants.ID_IS_NULL)));
    }

    @Test
    public void updateMovie_idIsNegative_ThrowException() throws Exception {
        //Given
        Movie movieToUpdate = new Movie.Builder()
            .id(-1L)
            .title("My new wonderful movie")
            .description("Some description here")
            .build();
        //When & Then
        mockMvc.perform(put("/api/movie")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(movieToUpdate)))
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.title", is(ErrorConstants.ID_IS_NEGATIVE)));
    }
}
