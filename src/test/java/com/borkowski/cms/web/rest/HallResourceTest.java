package com.borkowski.cms.web.rest;

import com.borkowski.cms.domain.Hall;
import com.borkowski.cms.domain.HallInfo;
import com.borkowski.cms.service.HallService;
import com.borkowski.cms.service.dto.HallDTO;
import com.borkowski.cms.service.dto.HallRowDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;

import java.io.IOException;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = HallResource.class, secure = false)
public class HallResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HallService hallService;


    @Test
    public void createHallTemplate_createNormally() throws Exception {
        //Given
        HallDTO hallDTO = new HallDTO.Builder()
            .hallInfo(new HallInfo.Builder().name("Sala 1").build())
            .rows(Arrays.asList(
                new HallRowDTO.Builder().numberOfSeats(20).build(),
                new HallRowDTO.Builder().numberOfSeats(20).build(),
                new HallRowDTO.Builder().numberOfSeats(20).build()
            ))
            .build();
        Hall returnedHall = new Hall(hallDTO);
        returnedHall.setId(1L);
        given(hallService.saveNewTemplate(any(HallDTO.class))).willReturn(returnedHall);
        //When & Then
        mockMvc.perform(post("/api/hall/template")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(hallDTO)))
            .andDo(print())
            .andExpect(status().isCreated())
            .andExpect(header().string("Location", containsString("/api/hall/template/" + returnedHall.getId().intValue())))
            .andExpect(jsonPath("$.hallInfo.name", is(hallDTO.getHallInfo().getName())))
            .andExpect(jsonPath("$.rows.length()", is(3)));
    }
}
