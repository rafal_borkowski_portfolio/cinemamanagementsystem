package com.borkowski.cms.service.impl;

import com.borkowski.cms.domain.Movie;
import com.borkowski.cms.repository.MovieRepository;
import com.borkowski.cms.service.MovieService;
import com.borkowski.cms.web.rest.errors.ErrorConstants;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

@RunWith(SpringRunner.class)
public class MovieServiceImplTest {

    @Mock
    private MovieRepository movieRepository;

    private MovieService movieService;

    @Captor
    private ArgumentCaptor<Long> idCaptor;

    @Captor
    private ArgumentCaptor<Movie> movieCaptor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        movieService = new MovieServiceImpl(movieRepository);
    }

    @Test
    public void saveMovie_saveNormally() {
        //Given
        Movie movieToSave = new Movie.Builder()
            .title("My new wonderful movie")
            .description("Some description here")
            .build();
        Movie savedMovie = new Movie(movieToSave);
        savedMovie.setId(1L);
        given(movieRepository.save(any(Movie.class))).willReturn(savedMovie);
        //When
        Movie newMovie = movieService.save(movieToSave);
        //Then
        then(movieRepository).should(times(1)).save(movieToSave);
        Assertions.assertThat(newMovie.getId()).isEqualTo(savedMovie.getId());
        Assertions.assertThat(newMovie.getDescription()).isEqualTo(savedMovie.getDescription());
        Assertions.assertThat(newMovie.getTitle()).isEqualTo(savedMovie.getTitle());
    }

    @Test
    public void saveMovie_idIsNotNull_throwException() {
        //Given
        Movie movieToSave = new Movie.Builder()
            .id(1L)
            .title("My new wonderful movie")
            .description("Some description here")
            .build();
        //When & Then
        Assertions.assertThatThrownBy(() -> movieService.save(movieToSave))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Id should be null");
    }

    @Test
    public void findMovieById_returnNormally() {
        //Given
        Movie movie = new Movie.Builder()
            .id(1L)
            .title("My new wonderful movie")
            .description("Some description here")
            .build();
        given(movieRepository.findById(movie.getId())).willReturn(Optional.of(movie));
        //When
        Optional<Movie> result = movieRepository.findById(movie.getId());
        //Then
        then(movieRepository).should(times(1)).findById(idCaptor.capture());
        Assertions.assertThat(idCaptor.getValue()).isEqualTo(movie.getId());
        Assertions.assertThat(result).isEqualTo(Optional.of(movie));
    }

    @Test
    public void findMovieById_IdIsNull_ThrowException() {
        //When & Then
        Assertions.assertThatThrownBy(() -> movieService.findById(null))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage(ErrorConstants.ID_IS_NULL);
    }

    @Test
    public void findMovieById_IdIsNegative_ThrowException() {
        //When & Then
        Assertions.assertThatThrownBy(() -> movieService.findById(-1L))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage(ErrorConstants.ID_IS_NEGATIVE);
    }

    @Test
    public void updateMovie_updateNormally() {
        //Given
        Movie movieToUpdate = new Movie.Builder()
            .id(1L)
            .title("My new wonderful movie")
            .description("Some description here")
            .build();
        given(movieRepository.save(movieToUpdate)).willReturn(movieToUpdate);
        //When
        Movie updatedMovie = movieService.update(movieToUpdate);
        //Then
        then(movieRepository).should(times(1)).save(movieCaptor.capture());
        Assertions.assertThat(movieCaptor.getValue()).isEqualTo(movieToUpdate);
        Assertions.assertThat(updatedMovie).isEqualTo(movieToUpdate);
    }

    @Test
    public void updateMovie_IdIsNull_ThrowException() {
        //Given
        Movie movieToUpdate = new Movie.Builder()
            .id(null)
            .build();
        //When & Then
        Assertions.assertThatThrownBy(() -> movieService.update(movieToUpdate))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage(ErrorConstants.ID_IS_NULL);
    }

    @Test
    public void updateMovie_IdIsNegative_ThrowException() {
        //Given
        Movie movieToUpdate = new Movie.Builder()
            .id(-1L)
            .build();
        //When & Then
        Assertions.assertThatThrownBy(() -> movieService.update(movieToUpdate))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage(ErrorConstants.ID_IS_NEGATIVE);
    }
}
