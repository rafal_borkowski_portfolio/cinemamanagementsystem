package com.borkowski.cms.service.impl;

import com.borkowski.cms.domain.Hall;
import com.borkowski.cms.domain.HallInfo;
import com.borkowski.cms.repository.HallRepository;
import com.borkowski.cms.service.HallService;
import com.borkowski.cms.service.dto.HallDTO;
import com.borkowski.cms.service.dto.HallRowDTO;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

public class HallServiceImplTest {

    @Mock
    private HallRepository hallRepository;

    private HallService hallService;

    @Captor
    private ArgumentCaptor<Long> idCaptor;

    @Captor
    private ArgumentCaptor<Hall> hallCaptor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        hallService = new HallServiceImpl(hallRepository);
    }

    @Test
    public void saveNewTemplate_saveNormally() {
        //Given
        HallDTO hallDTO = new HallDTO.Builder()
            .hallInfo(new HallInfo.Builder().name("Sala 1").build())
            .rows(Arrays.asList(
                new HallRowDTO.Builder().numberOfSeats(20).build(),
                new HallRowDTO.Builder().numberOfSeats(20).build(),
                new HallRowDTO.Builder().numberOfSeats(20).build()
            ))
            .build();
        given(hallRepository.save(any(Hall.class))).willReturn(null);
        //When
        hallService.saveNewTemplate(hallDTO);
        //Then
        then(hallRepository).should(times(1)).save(hallCaptor.capture());
        Assertions.assertThat(hallCaptor.getValue().getHallInfo().getName()).isEqualTo(hallDTO.getHallInfo().getName());
        Assertions.assertThat(hallCaptor.getValue().getTemplate()).isTrue();
        Assertions.assertThat(hallCaptor.getValue().getRows().size()).isEqualTo(3);
        Assertions.assertThat(hallCaptor.getValue().getRows().get(0).getRowNumber()).isEqualTo(1);
        Assertions.assertThat(hallCaptor.getValue().getRows().get(2).getRowNumber()).isEqualTo(3);
    }

    @Test
    public void findById_findNormally() {
        //Given
        given(hallRepository.findById(anyLong())).willReturn(null);
        //When
        hallService.findById(1L);
        //Then
        then(hallRepository).should(times(1)).findById(idCaptor.capture());
        Assertions.assertThat(idCaptor.getValue()).isEqualTo(1L);
    }

    @Test
    public void findById_idIsNull_ThrowException() {
        //When & Then
        Assertions.assertThatThrownBy(() -> hallService.findById(null))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Id must be defined");
    }

    @Test
    public void findByIdAndTemplateTrue_findNormally() {
        //Given
        given(hallRepository.findByIdAndIsTemplateTrue(anyLong())).willReturn(null);
        //When
        hallService.findByIdAndTemplateTrue(1L);
        //Then
        then(hallRepository).should(times(1)).findByIdAndIsTemplateTrue(idCaptor.capture());
        Assertions.assertThat(idCaptor.getValue()).isEqualTo(1L);
    }

    @Test
    public void findByIdAndTemplateTrue_idIsNull_ThrowException() {
        //When & Then
        Assertions.assertThatThrownBy(() -> hallService.findByIdAndTemplateTrue(null))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Id must be defined");
    }
}
