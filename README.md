# CinemaManagementSystem

### JHipster

W celu przyśpieszenia projektu, główny Core aplikacji został wygenerowany w programie JHipster.
Wygenerował on front-end w Angularze oraz back-end w Springu oraz konfiguracje do postawienia aplikacji na dokerze.
Więcej informacji o tym projekcie można znaleźć na stronie [JHipstera](https://www.jhipster.tech/)

### Model bazy danych

Ogólny zarys modelu bazy danych znajduje się poniżej:

![](model.png)

Projektując cały model, w głównej mierze skupiałem się na tym, aby system był jak najbardziej elastyczny.

Schemat modelu zawarty jest w projekcie w pliku `database_model.mwb` i został stworzony w programie MySQL Workbench,
jednakże cała struktura bazy danych została ręcznie opisana w plikach liquibase'owych.

#### Opis poszczególnych tabel

-   Movie - przechowuje informacje o wszystkich filmach które były, są bądź będą grane w danym kinie.
-   HallInfo - przechowuje ogólne informacje o salach dostępnych w kinie
-   Hall - jest łącznikiem pomiędzy poszczególnymi seansami a zajętymi miejscami
-   Screening - odzwierciedla poszczególne grane aktualnie filmy
-   HallRow - reprezentuje rzędy składające się na Sale kinowe.
-   Seat - Tutaj przechowywane są informacje o tym jakie miejsca są zajęte w jakich rzędach na jakie filmy, jeśli dokonywana jest rezerwacja to tutaj zostaje zapisane jakie miejsce zostało zajęte, przez kogo i na jaki bilet.
-   Ticket - Przechowuje rodzaje biletów
-   Reservation - przechowuje informacje kto dokonał jakich rezerwacji

Tego typu struktura umożliwia w dowolny sposób definiować ile miejsc jest w danej sali i każdy rząd może mieć inną liczbę miejsc.

Oprócz tego można dodawać nowe rodzaje biletów.

Warto także nadmienić, że przy takim modelu zapisywane są tylko dane o rezerwacjach, a nie wszystkie miejsca niezaleźnie od tego czy są zajęte czy też nie.

#### Flow

1. Administrator dodaje nowy film w Tabeli Movie
2. Jeśli jest potrzeba Administrator może dodać nową Salę i zdefiniować ile rzędów oraz ile miejsc w każdym rzędzie się znajduje. Tego typu sala będzie służyć jako schemat do przypisywania innych sal do seansów dlatego dla niego flaga isTemplate jest równa true;
3. Chcąc dodać nowy seans Administrator definiuje w jakiej sali będzie się on odgrywał, jakiego filmu dotyczy oraz kiedy będzie grany. Wybierając sale należy podać id sali która w Tabeli Hall ma flagę isTemplate ustawioną na true.
4. Po utworzeniu nowego seansu, pojawi się on na liście dostępnych seansów.
5. Użytkownik końcowy może teraz złożyć rezerwację na wskazane miejsca i wskazane rodzaje biletów.

Część z opisanych czynności ma odzwierciedlenie w RESTach które zostały opisane w dalszej części.

### Odpalanie projektu

Projekt zakłada pracę na bazie danych MySQL, dlatego aby móc go odpalić zalecałbym zainstalowanie owej bazy.
Po stworzeniu nowej bazy należy odpowiednio skonfigurować plik konfiguracyjny `src/main/resource/config/application-*.yml`,
domyślnie projekt będzie odpalał się w wersji developerskiej dlatego należy skonfigurować plik `application-dev.yml`

We właściwościach `datasource.url`, `datasource.username` oraz `datasource.password` należy podać ip, port oraz nazwę założonej bazy oraz podać ewentualne passy dla użytkownika na którego aplikacja będzie mogła się logować.

Jeśli konfiguracja do bazy danych będzie ustawiona prawidłowo to po odpaleniu projektu struktura bazy danych oraz dane testowe powinny zostać zainicjowane przez Liquibase'a

Przypuszczalnie do odpalenia projektu będzie potrzebny również `npm` jednakże aby to zweryfikować musiałbym postawić całe środowisko od początku.
Wygenerowany projekt w JHipsterze ma wbudowanego Maven Wrappera, dlatego aby odpalić aplikację wystarczy wywołać plik `mvnw` w konsoli.

Domyślnie back-end zostanie postawiony na `localhost:8080` i będzie można już z niego korzystać.

### Use Case'y

##### Użytkownik wybiera dzień oraz czas kiedy chciałby zobaczyć film, w odpowiedzi dostaje listę filmów, może przejrzeć konkretny film pod kątem wolnych miejsc

Do tego celu został stworzony rest który można wywołać za pomocą komendy:

    curl -X GET -H 'Content-Type: application/json'
    -v -i 'http://localhost:8080/api/screening?page=0&size=20&sort=movie.title%2Casc&playDate.greaterOrEqualThan=2019-06-01T17%3A00%3A00Z&playDate.lessOrEqualThan=2019-06-01T23%3A00%3A00Z'

Zwraca on Listę filmów w postaci strony.
Przekazując poniższe parametry w URLu można ograniczyć zakres wyszukiwania:

-   page - numer strony
-   size - ilość elementów na strone
-   sort - po jakim elemencie sortować
-   playDate.greaterOrEqualThan - data po której szukamy filmów
-   playDate.lessOrEqualThan - data do której szukamy filmów

Następnie po wywołaniu resta:

    curl -X GET -H 'Content-Type: application/json'  -v -i 'http://localhost:8080/api/screening/1'

Możemy uzykać informacje o filmie o id 1. Zostanie zwrócona informacja jakie miejsca zostały już zajęte.

W informacji zwrotnej dostajemy poszczególne rzędy dla danego filmu, ile jest w nich miejsc oraz które miejsca są już zajęte.

##### Użytkownik dokonuje rezerwacji komendą:

    curl -X POST -H 'Content-Type: application/json' -d '{"name":"Imie","surname":"Nazwisko-Nazwisko","seats": [{"rowId":10,"columnNumber":1,"ticketId":1},{"rowId":10,"columnNumber":2,"ticketId":2}]}' -v -i 'http://localhost:8080/api/reservation'

Przy tworzeniu rezerwacji należy podać:

-   `name` Imię - Imie musi zaczynać się wielką literą
-   `surname` Nazwisko - Nazwisko musi zaczynać się wielką literą oraz może być dwuczłonowe, wtedy oba człony muszą być przedzielone znakiem "-" oraz oba muszą zaczynać się wielką literą
-   `seats` Listę miejsc w postaci listy obiektów które chce się zarezerowawać, każdy obiekt musi składać się z:
    -   `rowId` Id rzędu w którym ma zostać zarezerowane miejsce
    -   `columnNumber` Id kolumny, inaczej miejsca które ma być zarezerwowane w rzędzie
    -   `ticketId` Id biletu informujący jaki rodzaj biletu będzie przypisany do tego miejsca

Podczass rezerwacji należy pamiętać, że nie mogą pozostać puste miejsce między rezerwowanymi miejscami a już zarezerwowanymi miejscami. Jeśli zajdzie taka sytuacja aplikacja zwróci odpowiedni błąd z informacją zwrotną.

Rezerwacje można dokonać maksymalnie 15 minut przed seansem, w innym przypadku zostanie zwrócony odpowidni błąd.

W nagłówku odpowiedzi o nazwie `Location` znajduję się również URL pod którym można znaleźć informacje dotyczące wykonanej rezerwacji. Przykładowe zapytanie poniżej:

    curl -X GET -H 'Content-Type: application/json' -v -i 'http://localhost:8080/api/reservation/1'

Na końcu URLa podajemy `id` rezerwacji którą chcemy podejrzeć. Oficjalnie Id nie powinno być tak pospolite lub do wyszukiwania rezerwacji powinno posłużyć inne pole z pseudo-losowym kluczem aby ich przeglądanie nie było takie proste albo powyższy URL powinien być bardziej złożony, gdzie w ciele zapytania wysyłalibyśmy więcej danych dotyczących rezerwacji. W innym przypadku moglibyśmy narazić system na niepotrzebny wyciek danych, gdyż potencjalny haker mógłby przeszukać bazę wywołując kolejne id rezerwacji.

```
    UWAGA OD AUTORA
    1. Umknęła mi gdzieś informacja o czasie wygaśnięcia rezerwacji
    i nie zostało to zaimplementowane.
    2. System nie wylicza łącznej kwoty po stronie serwera, tylko zwraca informacje
    o poszczególnych miejscach i ich cenach, tak wiec cene końcową sugerowałbym wyliczać
    na froncie aby klienci wiedzieli ile płacą za poszczególne bilety. Ewentualnie mógłbym
    dodać jedno dodatkowe pole w obiekcie zwracanym przy pobieraniu informacji o rezerwacji
    które zwracałoby sumę cen biletów, ale to już mały szczegół związany z dodawaniem BigDecimalów
```

#### Dodatkowe resty umożliwiające prace

Większość RESTów umożliwiających zarządzanie salami, filmami itp. wymaga autentykacji admina, dlatego aby uzyskać token autentykacyjny należy wywołać komendę:

    curl -X POST -H 'Content-Type: application/json' -d '{
        "username":"admin",
        "password":"admin"
    }' -v -i 'http://localhost:8080/api/authenticate'

Następnie uzyskany token należy wkleić do wszystkich zapytań które go wymagają w formie nagłówka zapytania http `authorization: Bearer token`.
W komendach poniżej nagłówek ten jest juz wklejony, należy jedynie wkleić uzyskany token w miejsce pola `token`

#### Dodanie nowego filmu

    curl -X POST -H 'Content-Type: application/json' -H 'authorization: Bearer token' -d '{
        "title": "Szybcy i Wścielki",
        "description": "Te pościgi, we wybuchy"
    }' -v -i 'http://localhost:8080/api/movie'

W body przesyłany nowy obiekt filmu podając pola

-   `title` Tytuł filmu
-   `description` Opis filmu, to pole jest opcjonalne

Możemy także edytować filmy wysyłając powyższe zapytanie z czasownikiem `PUT` oraz dodając pole `id` definiujące jaki film chcemy edytować

Wywołując komendę:

    curl -X GET -H 'Content-Type: application/json' -H 'authorization: Bearer token' -v -i 'http://localhost:8080/api/movie/1'

Możemy uzyskać informacje o konkretnym filmie podając jego `id` na końcu URLu w miejsce `1`

#### Tworzenie nowej sali

    curl -X POST -H 'Content-Type: application/json' -H 'authorization: Bearer token' -d '{
        "hallInfo":{
            "name":"Nazwa sali"
        },
        "rows":[
            {
                "numberOfSeats":20
            },
            {
                "numberOfSeats":20
            },
            {
                "numberOfSeats":20
            },
            {
                "numberOfSeats":20
            },
            {
                "numberOfSeats":20
            }
            ]
    }' -v -i 'http://localhost:8080/api/hall/template'

Opis poszczególnych pól:

-   `hallInfo` Obiekt przetrzymujący informacje o sali, aktualnie składa się na niego tylko nazwa w postaci parametru `name`
-   `rows` lista rzędów przypadających na daną sale, aktualnie każdy rząd definiuje jedno pole `numberOfSeats` definiujące ilość miejsc w danym rzędzie

Po utworzeniu nowej sali, informacje o niej oraz rzędy dodadzą się kaskadowo, oraz flaga `isTemplate` zostanie oznaczona na `true`.

Po dodaniu nowej sali można wykorzystać ją jako szablon do tworzenia nowych seansów.

#### Tworzenie nowego seansu

    curl -X POST -H 'Content-Type: application/json' -H 'authorization: Bearer token' -d '{
        "movieId":1,
        "playDate":"2019-06-01T11:00:00Z",
        "hallId":1
    }' -v -i 'http://localhost:8080/api/screening'

Przekazywane parametry w body:

-   `movieId` id filmu do którego ma być przypisany dany seans
-   `playDate` data wyswietlania filmy w postaci Timestampa
-   `hallId` id sali w której będzie miał miejsce dany seans. Ważne aby id podanej sali odnosiło się do sali która jest szablonem, czyli flagę `isTemplate` ma na `true`. W innym przypadku aplikacja zwróci odpowiedni błąd.

#### Dodanie nowego rodzaju biletów

    curl -X POST -H 'Content-Type: application/json' -H 'authorization: Bearer token' -d '{
        "name":"Nazwa biletu",
        "price":50.0
    }' -v -i 'http://localhost:8080/api/ticket'

Przekazywane parametry w body:

-   `name` nazwa nowego biletu
-   `price` jego cena jako wartość zmiennoprzecinkowa

### Uwagi końcowe

#### Testy

Zdaje sobie sprawę z tego, że testy są bardzo ważne jednakże, nie miałem czasu za bardzo
pokrywać całego kodu testami, zwłaszcza, że projekt najprawdopodobniej nie będzie przeze mnie
dalej utrzymywany, dlatego napisałem tylko kilka testów jednostkowych i integracyjnych
do serwisów i restów w celu pokazania, że wiem jak się je mniej więcej robi.
Osobiście znam frameworki takie jak: **Junit 4,5, Mockito i PowerMockito, AssertJ i Hamcrest**
oraz korzystałem z SonarQube oraz PitTest (testy mutacyjne). Niestety w aktualnej pracy nie piszemy testów
na bierząco, dlatego nie mam z nimi zbyt dużego doświadczenia ale staram się to zmienić i w projekcie
który tworzę po godzinach staram się pracować w TDD.

#### Smrodki

Zdaje sobie sprawę również z tego, że niektóre zaimplementowane funkcjonalności wymagają udoskonalenia
(jak np. clasa ExceptionTranslator w której jest kaskada ifów, lub przekazywane całe encje zamiast DTOsów i wiele innych), jednakże,
poświęciłem na ten projekt około 20 godzin i wydaję mi się, że wynika już z
niego co umiem, a czego nie umiem więc nie chciałbym poświęcam mu już więcej czasu
(przynajmniej teraz), gdyż mam inne sprawy na głowe jak magisterka czy projekt
o którym wspomniałem wyżej.

# JHipster

Wszysztkie informacje dotyczące JHipstera które zostają wygenerowane wraz z projektem można znaleźć [tutaj](informaton-generated-by-JHipster.md);
